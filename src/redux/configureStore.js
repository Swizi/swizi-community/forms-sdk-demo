import {applyMiddleware, createStore} from 'redux'
import {rootReducer} from './rootReducer'
import ReduxThunk from 'redux-thunk'

const middleware = applyMiddleware(ReduxThunk)

export default function configureStore (initialState) {
  return createStore(
    rootReducer,
    initialState,
    middleware,
  )
}
