/* @flow */

import { Map, List } from 'immutable'

var allActions = {}
let ACTION_HANDLERS = {}

//                    Start Spinner
export const START_SPINNER = 'START_SPINNER'
const startSpinner = (dispatch): Action => {

  return {
    type: START_SPINNER,
    payload: {
      dispatch: dispatch,
    },
  }
}
ACTION_HANDLERS[[START_SPINNER]] = function (state, action) {
  let dispatch = action.payload.dispatch
  let taskCount = state.taskCount + 1

  return {
    ...state,
    taskCount,
  }
}
allActions = {...allActions, startSpinner}

//                    Stop Spinner
export const STOP_SPINNER = 'STOP_SPINNER'
const stopSpinner = (dispatch): Action => {

  return {
    type: STOP_SPINNER,
    payload: {
      dispatch: dispatch,
    },
  }
}
ACTION_HANDLERS[[STOP_SPINNER]] = function (state, action) {
  let dispatch = action.payload.dispatch
  let taskCount = state.taskCount - 1

  return {
    ...state,
    taskCount: (taskCount > 0) ? taskCount : 0,
  }
}
allActions = {...allActions, stopSpinner}

const initialState = {
  modal: new Map({
    isOpen: false,
  }),
  taskCount: 0,
  currentTasks: new List(),

}

export const actions = {...allActions}

export default function ui (state, action: Action): object {
  const handler = ACTION_HANDLERS[action.type]
  // Si le state ne contient qu'un field c'est que c'est le token. faut initialiser.
  if (Object.keys(state).indexOf(Object.keys(initialState)[0]) === -1)
    state = {...initialState, ...state}
  return handler ? handler(state, action) : state
}
