/**
 * Created by olivierraveneau on 18/03/2017.
 */

'use strict'

function minimalistState (data, state) {
  let copyData = JSON.parse(JSON.stringify(data))
  Object.keys(copyData).forEach(k => {
    if (Object.keys(state).indexOf(k) != -1)
      delete copyData[k]
  })

  return copyData
}

export function combineReducers (reducers = {}, universalStates) {
  var reducerKeys = Object.keys(reducers)
  return function combination (state = {}, action) {
    var hasChanged = false
    var nextState = {}
    let combinedState

    for (let i = 0; i < reducerKeys.length; i++) {
      let key = reducerKeys[i]
      let theState = state[key] || {}

      Object.keys(universalStates).forEach(reducer => {
        if (key != reducer)
          universalStates[reducer].forEach(
            value => {
              if (!theState[reducer])
                theState[reducer] = {}
              if (state[reducer])
                theState[reducer][value] = state[reducer][value]
            }
          )

      })

      combinedState = {...theState}

      nextState[key] = reducers[key](combinedState, action)
      hasChanged = hasChanged || nextState[key] !== state[key]
    }
    return hasChanged ? nextState : state
  }
}
