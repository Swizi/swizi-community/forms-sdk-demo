import React from 'react'
import { actions } from '../redux/rootReducer'

class ReactBaseComponent extends React.Component {

  componentDidMount () {
    this.setState({triggerRefresh: true})
  }

  forceRefresh () {
    this.setState({triggerRefresh: (this.state.triggerRefresh ? (!this.state.triggerRefresh) : true)})
  }

  _bind (...methods) {
    var that = this
    methods.forEach(function (method) {
      if (that.__proto__.hasOwnProperty(method))
        that[method] = that[method].bind(that)

    })
  }

  openDialog (type, title, text, emitter, data) {
    this.call('openDialog', type, title, text, emitter, data, this.constructor.name)

  }

  hasDialogResult (prevProps, emitter) {
    if (prevProps.modal !== this.props.modal)
      if (this.props.modal.get('emitter') === emitter)
        if (this.props.modal.get('isOpen') === false)
          return true

    return false
  }

  consumeDialogResult () {
    if (this.props.modal.get('classEmitter') === this.constructor.name)
      if (this.props.modal.get('isOpen') === false)
        this.call('resetDialog')
  }

  call (action, ...parameters) {
    try {
      this.props.dispatch(actions[action](...parameters, this.props.dispatch))
    } catch (e) {
    }
  }
}

export default ReactBaseComponent
