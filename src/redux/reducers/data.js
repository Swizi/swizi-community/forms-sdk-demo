/* @flow */
import { List } from 'immutable'
import moment from 'moment'
import { actions as uiActions } from './ui'
import FormsSdk from 'swizi-forms-sdk'

const FormsClient = FormsSdk.FormsClient

let formsClient
let debug = (message, data) => {
  if (__ENABLE_DEVTOOLS__)
    debug(message, data)
  else
    swizi.log('info', JSON.stringify(data))
}

function handleDataUpdated (queryOrViewNames, eventType, dispatch,) {

  if (queryOrViewNames.indexOf('myForm Data') > -1 && eventType === FormsClient.events.QUERY_SUCCESS) {
    // The form has just been updated
    dispatch(dataUpdated(formsClient.getData('myForm Data'), dispatch))
    dispatch(refreshPendingIndicators(dispatch))
  }

  if (eventType === FormsClient.events.QUERY_DELAYED) {
    // A query has been delayed, refresh pending counters
    dispatch(refreshPendingIndicators(dispatch))
  }

  if ((queryOrViewNames.indexOf('add an item') > -1 || queryOrViewNames.indexOf('update an item') > -1) && eventType === FormsClient.events.QUERY_SUCCESS) {
    // An item has just been created or updated by me on server, refresh data
    dispatch(updateData(undefined, undefined, dispatch))
  }

  if (eventType === FormsClient.events.FETCH_STARTED)
    dispatch(uiActions.startSpinner(dispatch))

  if (eventType === FormsClient.events.FETCH_STOPPED)
    dispatch(uiActions.stopSpinner(dispatch))
}

let allActions = {}
let ACTION_HANDLERS = {}

//                    Load Cache
export const LOAD_CACHE = 'LOAD_CACHE'
const loadCache = (dispatch): Action => {

  return {
    type: LOAD_CACHE,
    payload: {
      dispatch: dispatch,
    },
  }
}
ACTION_HANDLERS[[LOAD_CACHE]] = function (state, action) {
  let dispatch = action.payload.dispatch
  let promise

  // Create form client
  if (__ENABLE_DEVTOOLS__)
    promise = FormsClient.fromLocalStorage('n5exdAZP3SfNkMU2rHKPs5Zbq1Tqxq1vAFrZHQ16', 'Z4dCFwzkxUxfB2E35CUWQc0Gp', 'https://ms-forms.swizi.io', 'formsSDKTest')
  else
    promise = FormsClient.fromSwizi('forms')

  promise.then(fc => {
    formsClient = fc
    // Subscribe to defer updates
    formsClient.addListener((queryOrViewNames, eventType) => {handleDataUpdated(queryOrViewNames, eventType, dispatch)})
    dispatch(cacheLoaded(dispatch))
    dispatch(refreshPendingIndicators(dispatch))
    dispatch(updateData(dispatch))

    setInterval(() => {
      dispatch(changeLastUpdate(Math.floor((new Date().getTime() - formsClient.getUpdatedAt()) / 1000)), dispatch)
    }, 1000)
  })
    .catch(e => {
      swizi.log('info', 'La creation a echoué : ' + e.message)
    })
  return {
    ...state,
  }
}
allActions = {...allActions, loadCache}

//                    Cache Loaded
export const CACHE_LOADED = 'CACHE_LOADED'
const cacheLoaded = (dispatch): Action => {

  return {
    type: CACHE_LOADED,
    payload: {
      dispatch: dispatch,

    },
  }
}
ACTION_HANDLERS[[CACHE_LOADED]] = function (state, action) {
  let dispatch = action.payload.dispatch

  return {
    ...state,
    fieldList: new List(formsClient.getData('myForm Data'))
  }
}
allActions = {...allActions, cacheLoaded}

//                    Refresh Pending Indicators
export const REFRESH_PENDING_INDICATORS = 'REFRESH_PENDING_INDICATORS'
const refreshPendingIndicators = (dispatch): Action => {

  return {
    type: REFRESH_PENDING_INDICATORS,
    payload: {
      dispatch: dispatch,
    },
  }
}
ACTION_HANDLERS[[REFRESH_PENDING_INDICATORS]] = function (state, action) {
  return {
    ...state,
    pendingCreation: formsClient.getPendingQuery(FormsClient.queryType.CREATE).length,
    pendingUpdate: formsClient.getPendingQuery(FormsClient.queryType.UPDATE).length,
  }
}
allActions = {...allActions, refreshPendingIndicators}

//                    Update Data
export const UPDATE_DATA = 'UPDATE_DATA'
const updateData = (callbackOK, callbackKO, dispatch): Action => {

  return {
    type: UPDATE_DATA,
    payload: {
      dispatch: dispatch,
      callbackOK,
      callbackKO,
    },
  }
}

ACTION_HANDLERS[[UPDATE_DATA]] = function (state, action) {
  let dispatch = action.payload.dispatch

  swizi.log('info', 'try update data')

  if (!formsClient)
    swizi.log('info', 'formsClient est null !')

  if (!formsClient.exists('myForm Data')) {
    swizi.log('info', 'no previous data')
    formsClient.createView('myForm Data', 'SampleForm', {})
  } else
    formsClient
      .refreshView('myForm Data')
      .then(() => {
        if (action.payload.callbackOK) {
          action.payload.callbackOK()
        }
      })
      .catch(e => {
        if (action.payload.callbackKO)
          action.payload.callbackKO()
      })

  return {
    ...state,
  }
}
allActions = {...allActions, updateData}

//                    Data updated
export const DATA_UPDATED = 'DATA_UPDATED'
const dataUpdated = (data, dispatch): Action => {
  return {
    type: DATA_UPDATED,
    payload: {
      dispatch: dispatch,
      data
    },
  }
}
ACTION_HANDLERS[[DATA_UPDATED]] = function (state, action) {
  let dispatch = action.payload.dispatch
  let data = action.payload.data
  let oldValues = state.fieldList.toArray()

  data.forEach((field, idx) => {
    if (idx >= oldValues.length || field.value !== oldValues[idx].value)
      field.hasChanged = true
  })

  return {
    ...state,
    fieldList: new List(data),
  }
}
allActions = {...allActions, dataUpdated}

//                    Add
export const ADD = 'ADD'
const add = (dispatch): Action => {

  return {
    type: ADD,
    payload: {
      dispatch: dispatch,
    },
  }
}
ACTION_HANDLERS[[ADD]] = function (state, action) {

  let dispatch = action.payload.dispatch
  let field = 'field ' + moment().format('HH:mm')
  let value = Math.ceil(Math.random() * 100, 0)

  swizi.log('info', 'Try to update.')
  formsClient
    .createRecord('add an item', 'SampleForm', {field, value})
    .then(result => {
    })
    .catch(() => {

    })

  return {
    ...state,
  }
}
allActions = {...allActions, add}

//                    Update
export const UPDATE = 'UPDATE'
const update = (dispatch): Action => {

  return {
    type: UPDATE,
    payload: {
      dispatch: dispatch,
    },
  }
}
ACTION_HANDLERS[[UPDATE]] = function (state, action) {
  let dispatch = action.payload.dispatch
  // update a random field
  let idxField = Math.floor(Math.random() * (formsClient.getData('myForm Data').length - 1))
  let id = formsClient.getData('myForm Data')[idxField].id

  formsClient
    .updateRecord('update an item', 'SampleForm', {id}, {value: 'Updated at ' + moment().format('HH:mm:ss')})
    .then(() => {

    })

  return {
    ...state,
  }
}
allActions = {...allActions, update}

//                    Change Last Update
export const CHANGE_LAST_UPDATE = 'CHANGE_LAST_UPDATE'
const changeLastUpdate = (lastUpdate, dispatch): Action => {

  return {
    type: CHANGE_LAST_UPDATE,
    payload: {
      dispatch: dispatch,
      lastUpdate

    },
  }
}
ACTION_HANDLERS[[CHANGE_LAST_UPDATE]] = function (state, action) {
  let dispatch = action.payload.dispatch
  let lastUpdate = action.payload.lastUpdate

  return {
    ...state,
    lastUpdate,
  }
}
allActions = {...allActions, changeLastUpdate}

const initialState = {
  fieldList: new List([]),
  pendingCreation: 0,
  pendingUpdate: 0,
  lastUpdate: new Date().getTime(),
}

export const actions = {...allActions}

export default function data (state, action: Action): object {
  const handler = ACTION_HANDLERS[action.type]
  // Si le state ne contient qu'un field c'est que c'est le token. faut initialiser.
  if (Object.keys(state).indexOf(Object.keys(initialState)[0]) === -1)
    state = {...initialState, ...state}
  return handler ? handler(state, action) : state
}
