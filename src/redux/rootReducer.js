import { combineReducers } from './combineReducers'
import { routerReducer } from 'react-router-redux'

import ui, { actions as a1 } from './reducers/ui'
import data, { actions as a2 } from './reducers/data'


const rootReducer = combineReducers({
  routing: routerReducer,
  ui,
  data,
}, {

})

const actions = {
  ...a1,
  ...a2,
}

export { rootReducer, actions }
