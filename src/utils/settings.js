import swizi from "swizi";

export default function getSettings() {
    return new Promise((resolve, reject) => {
        swizi
            .getItems('sps')
            .then(items => {
                let conf = items.find(i => {
                    return i.label === 'sps'
                })
                if (!conf)
                    throw "sps group not found"

                conf = conf.list

                let settings = {}
                settings.server = conf.find(i => {
                    return i.key === 'server'
                }).value
                settings.apikey = conf.find(i => {
                    return i.key === 'apikey'
                }).value
                settings.secretkey = conf.find(i => {
                    return i.key === 'secretkey'
                }).value

                resolve(settings)
            })
            .catch(e => {
                reject(e)
            })
    })
}
