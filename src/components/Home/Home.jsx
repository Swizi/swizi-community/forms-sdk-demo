import React from 'react'
import { connect } from 'react-redux'

import './Home.less'
import ReactBaseComponent from '../ReactBaseComponent'
import PropTypes from 'prop-types'

class Home extends ReactBaseComponent {
  constructor () {
    super()
  }

  componentDidMount () {
  }

  render () {
    let pathname = this.props.history.location.pathname
    let action = this.props.history.action

    return (
      <div className="mainWrapper">
        <div className="home-container column centered aligned justified">
          <div
            className={
              action === 'POP' ? 'enterDown image-container' : 'enterDown image-container'
            }
          >
            <img className="logo" src="assets/images/logo.png"/>
          </div>
          {
            <div
              className={
                action === 'POP' ? 'enterUp button-wrapper' : 'button-wrapper'
              }
            >

              <button className="button-validate" onClick={() => {
                this.props.history.push('/formScreen')
              }}>
                <div className="row justified aligned">
                  <span>Commencer !</span>
                </div>
              </button>
            </div>
          }
        </div>
      </div>
    )
  }
}

Home.propTypes = {
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
}

const mapStateToProps = ({
                           ui: {taskCount},
                         }) => ({
  taskCount,
})

const mapDispatchToProps = (dispatch, apps) => ({
  dispatch: dispatch,
})

export default connect(mapStateToProps, mapDispatchToProps)(Home)
