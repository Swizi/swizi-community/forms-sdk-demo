import React from 'react'
import { Route } from 'react-router-dom'
import PropTypes from 'prop-types'

export const childFactoryCreator = classNames => child =>
    React.cloneElement(child, {
        classNames,
    })
export const getPathDepth = location => {
    let pathArr = (location || {}).pathname.split('/')
    pathArr = pathArr.filter(n => n !== '')
    return pathArr.length
}
export const renderMergedProps = (component, ...rest) => {
    const finalProps = Object.assign({}, ...rest)
    return React.createElement(component, finalProps)
}
export const PropsRoute = ({ component, ...rest }) => {
    return (
        <Route
            {...rest}
            render={routeProps => {
                return renderMergedProps(component, routeProps, rest)
            }}
        />
    )
}
PropsRoute.propTypes = {
    component: PropTypes.func.isRequired,
}
