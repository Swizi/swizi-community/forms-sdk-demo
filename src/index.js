import React from 'react'
import {render} from 'react-dom'
import {Provider} from 'react-redux'
import {HashRouter} from 'react-router-dom'

import './styles/styles.less'
import LOG from './utils/logger'
import './style.less'

import AppContainer from './containers/AppContainer'
import swizi from 'swizi'

import configureStore from './redux/configureStore'

const store = configureStore()

LOG('App starting in ' + NODE_ENV + ' mode', 'warn')

if (__ENABLE_DEVTOOLS__) {
    document.addEventListener('swiziReady', function () {
        swizi
            .getPlatform()
            .then(platform => {
                let clazz = 'main-android'

                if (platform === 'ios')
                    clazz = 'main-ios'

                render(
                    <div className={clazz}>
                        <Provider store={store}>
                            <HashRouter>
                                <AppContainer/>
                            </HashRouter>
                        </Provider>
                    </div>,
                    document.getElementById('root')
                )
            })

    })

} else {
    swizi
        .getPlatform()
        .then(platform => {
            let clazz = 'main-android'

            if (platform === 'ios')
                clazz = 'main-ios'
            render(
                <div className={clazz}>
                    <Provider store={store}>
                        <HashRouter>
                            <AppContainer/>
                        </HashRouter>
                    </Provider>
                </div>,
                document.getElementById('root')
            )
        })
}
