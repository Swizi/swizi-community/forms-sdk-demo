import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { Switch, withRouter } from 'react-router-dom'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import { childFactoryCreator, getPathDepth, PropsRoute } from '../utils/router'
import Home from '../components/Home'
import FormScreen from '../components/FormScreen'
import { actions } from '../redux/rootReducer'

import swizi from 'swizi'

class AppContainer extends React.Component {
  state = {
    prevDepth: getPathDepth(this.props.location),
  }

  componentWillMount () {
   // swizi.setFullscreen()
  }

  componentDidMount () {
    this.props.dispatch(actions.loadCache(this.props.dispatch))
  }

  componentWillReceiveProps (nextProps) {
    this.setState({prevDepth: getPathDepth(this.props.location)})
  }

  render () {
    return (
      <TransitionGroup
        childFactory={childFactoryCreator(
          getPathDepth(this.props.location) - this.state.prevDepth >= 0 ? 'pageSliderLeft' : 'pageSliderRight'
        )}
      >
        <CSSTransition
          key={this.props.location.pathname}
          timeout={600}
          classNames={
            getPathDepth(this.props.location) - this.state.prevDepth >= 0
              ? 'pageSliderLeft'
              : 'pageSliderRight'
          }
          mountOnEnter={true}
          unmountOnExit={true}
        >
          <Switch location={this.props.location}>
            <PropsRoute
              path="/"
              exact
              component={Home}/>
            <PropsRoute
              path="/formScreen"
              exact
              component={FormScreen}/>
          </Switch>
        </CSSTransition>
      </TransitionGroup>
    )
  }
}

const mapStateToProps = ({authReducer, visitReducer}) => ({
  authData: authReducer,
  visitsData: visitReducer,
})

const mapDispatchToProps = dispatch => ({
  dispatch: dispatch,

})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AppContainer))

AppContainer.propTypes = {
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,

}
