import React from 'react'
import { connect } from 'react-redux'

import './FormScreen.less'
import ReactBaseComponent from '../ReactBaseComponent'
import PropTypes from 'prop-types'
import ReactPullToRefresh from 'react-pull-to-refresh'

class FormScreen extends ReactBaseComponent {
  constructor () {
    super()
    this._bind('handlePullToRefresh')
  }

  handlePullToRefresh (resolve, reject) {
    this.call('updateData', resolve, reject)
  }

  renderBottomBar () {
    return (
      <div className="enterUp bottom-container row justified">
        <div className="pending-block">
          <div className="value">
            {this.props.pendingCreation}
          </div>
          <div className="title centered">
            Pending creations
          </div>
        </div>
        <div className="pending-block">
          <div className="value">
            {this.props.pendingUpdate}
          </div>
          <div className="title centered">
            Pending updates
          </div>
        </div>
      </div>
    )
  }

  renderRow (item, idx) {
    return (
      <div className={'data-row ' + (item.hasChanged ? 'has-changed' : '')} key={idx}>
        <div className="col1">
          {item.field}
        </div>
        <div className="col2">
          {item.value}
        </div>
      </div>
    )
  }

  renderData () {
    return (
      <div className="data-container">
        <div className="data-header">
          <div className="col1">
            Field
          </div>
          <div className="col2">
            Value
          </div>
        </div>
        <div className="data-content">
          <div className="data-rows">
            {this.props.fieldList.map((item, idx) => {return this.renderRow(item, idx)})}
          </div>
        </div>
      </div>
    )
  }

  renderButtons () {
    return (
      <div className="action-container around row">
        <button className="material-button button-validate enterLeft" onClick={() => {this.call('add')}}>Create</button>
        <button className="material-button button-validate enterRight" onClick={() => {this.call('update')}}>Update
        </button>
      </div>
    )
  }

  renderSpinner () {
    if (this.props.taskCount > 0)
      return (
        <div className="sk-fading-circle">
          <div className="sk-circle1 sk-circle"/>
          <div className="sk-circle2 sk-circle"/>
          <div className="sk-circle3 sk-circle"/>
          <div className="sk-circle4 sk-circle"/>
          <div className="sk-circle5 sk-circle"/>
          <div className="sk-circle6 sk-circle"/>
          <div className="sk-circle7 sk-circle"/>
          <div className="sk-circle8 sk-circle"/>
          <div className="sk-circle9 sk-circle"/>
          <div className="sk-circle10 sk-circle"/>
          <div className="sk-circle11 sk-circle"/>
          <div className="sk-circle12 sk-circle"/>
        </div>
      )
  }

  render () {
    let pathname = this.props.history.location.pathname
    let action = this.props.history.action

    return (
      <div className="screen1-container">
        <ReactPullToRefresh onRefresh={this.handlePullToRefresh}>
          <div className="header-container enterDown">
            <div className="back-arrow">
              <img src="assets/images/back.png" onClick={() => this.props.history.goBack()}/>
            </div>

            <div className="logo">
              <img src="assets/images/logo.png" alt=""/>
            </div>
            <div className="spinner">
              {this.renderSpinner()}
            </div>

          </div>
          {this.renderData()}
          {this.renderBottomBar()}
          {this.renderButtons()}
          <div className="time">
            {'Queue checked ' + this.props.lastUpdate + 's ago'}
          </div>
        </ReactPullToRefresh>

      </div>
    )
  }
}

FormScreen.propTypes = {
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
}

const mapStateToProps = ({
                           ui: {taskCount},
                           data: {fieldList, pendingCreation, pendingUpdate, lastUpdate}
                         }) => ({
  taskCount,
  fieldList: fieldList.toArray(),
  pendingCreation,
  pendingUpdate,
  lastUpdate,
})

const mapDispatchToProps = (dispatch, apps) => ({
  dispatch: dispatch,
})

export default connect(mapStateToProps, mapDispatchToProps)(FormScreen)
